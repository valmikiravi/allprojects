'''
Created on Aug 13, 2017

@author: Ravikumar V
'''

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import *
from selenium.webdriver.support.ui import Select
from selenium.webdriver.remote.webelement import WebElement

class CommActionsclass(object):
    
    def __init__(self,driver):
        self.driver=driver
        
        '''
    This function is to wait for the element . the waiting time can be changed according to the requirement. 
    This is n Explicit wait using Fluent wait
        '''
    def ElementwithFluentwait(self,locator,timeoutinseconds):
        wait = WebDriverWait(self.driver, timeoutinseconds, poll_frequency=1, ignored_exceptions=[ElementNotVisibleException, ElementNotSelectableException])
        elementdisplayed = wait.until(EC.presence_of_element_located((By.XPATH, locator)))
     
    '''
    this function is to select an option of the dropdown based on the visible text of the option
    '''   
    def selectfromDropdown(self,WebElement,dropdownOptionVisibletext):
        sel = Select(WebElement)
        sel.select_by_visible_text(dropdownOptionVisibletext)
    
    '''
    this function is to select an option of the dropdown based on the index of the option
    '''
    def selectbyIndexfromDropdown(self,WebElement,whichindex):
        sel = Select(WebElement)
        sel.select_by_index(whichindex)
    
    
    '''
    this function is to click on webelement. It takes the webelement as a parameter
    '''
    def clickonWebElement(self,WebElement):
        WebElement.click()
     
     
    '''
    this function is to enter text into the text box.It takes the webelement and text to be entered as  parameters 
    '''   
    def enterTextInTextBox(self,WebElement,Text):
        WebElement.click()
        WebElement.clear()
        WebElement.send_keys(Text)
    def getTextBox(self,WebElement,Text):
        WebElement.click()
        WebElement.clear()
        WebElement.text    
    
    def getText(self,WebElement):
        texT=WebElement.text
        return texT
    
    def checkifElemntisDisplayed(self,ele):
        if(ele.is_displayed()):
            return True
        else:
            return False
    
        
            
