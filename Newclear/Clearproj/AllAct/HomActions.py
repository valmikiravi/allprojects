'''
Created on Aug 20, 2017

@author: Ravikumar V
'''
from AllAct.CommActions import CommActionsclass
from AllPage.Homloc import Homlocclass
import time

class HomActionsclass(object):
    def __init__(self,driver):
        self.driver=driver
        
    def clickonyourtrips(self):
        locobj=Homlocclass(self.driver)
        ca=CommActionsclass(self.driver)
        ele=locobj.yourtripsloc()
        ca.clickonWebElement(ele)
        time.sleep(3)
    def clickontravellersAcc(self):
        locobj=Homlocclass(self.driver)
        ca=CommActionsclass(self.driver)
        ele=locobj.travellersloc()
        ca.clickonWebElement(ele)
        time.sleep(3)    
        
    def clickonsignin(self):
        locobj=Homlocclass(self.driver)
        ca=CommActionsclass(self.driver)
        ele=locobj.signinloc()
        ca.clickonWebElement(ele)  
        
    def enterusername(self,UnAme):
        ca=CommActionsclass(self.driver)
        locobj=Homlocclass(self.driver)
        ele=locobj.usernametext()
        print "going to enter text"
        ca.enterTextInTextBox(ele,UnAme)
         
    def enterpassword(self,pass_word1):
        ca=CommActionsclass(self.driver)
        locobj=Homlocclass(self.driver)
        ele=locobj.passwordtext()
        ca.enterTextInTextBox(ele,pass_word1)
         
    def clickonwindowsignin(self):
        locobj=Homlocclass(self.driver)
        ca=CommActionsclass(self.driver)
        ele=locobj.windowsignin()
        ca.clickonWebElement(ele)                    