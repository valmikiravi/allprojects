*** Settings ***
Library           Selenium2Library
Library           keysdef.py

*** Test Cases ***
TC01 Display the user trips and travellers name
    Given I am on the page URL    https://www.cleartrip.com
    clickonyourtrips
    clickonsignin
    enterusername
    enterpassword
    clickonwindowsignin
    clickonyourtrips
    clickontrips
    getTrips
    clickonyourtrips
    signout
    clickonyourtrips
    clickonsignin
    enterusername
    enterpassword
    clickonwindowsignin
    clickonyourtrips
    clickontravellers
    getTravellers
