'''
Created on Aug 20, 2017

@author: Ravikumar V
'''
from selenium import webdriver
import time
#from AllNavigations.HomeBusiness import HomeBusinessclass
from AllNavi.AccBusi import AccBusiclass
from AllNavi.HomBusi import HomBusiclass
from robot.api.deco import keyword


class keysdef():
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    def __init__(self):
        '''
        Constructor
        '''
        #self.driver=driver
        print "My Own Keywords"
    
    @keyword('Given I am on the page URL')
    @keyword('Given I am on Google page')
    @keyword('Given I am on Homepage')
    def AccessToHomepage(self,urlStRinG):
        print urlStRinG
        self.driver = webdriver.Chrome(executable_path="C:\\chromedriver.exe")
        self.driver.maximize_window()
        self.driver.get(urlStRinG)
        
    def clickonyourtrips(self):
        hb=HomBusiclass(self.driver)
        hb.clickonyourtrips() 
           
    def clickonsignin(self):
        hb=HomBusiclass(self.driver)
        hb.clickonsignin()
        
        
    def enterusername(self,UnamE):
        hb=HomBusiclass(self.driver)
        hb.enterusername(UnamE)
        
    def enterpassword(self,pass_word1):
        hb=HomBusiclass(self.driver)
        hb.enterpassword(pass_word1)
         
    def clickonwindowsignin(self):
        hb=HomBusiclass(self.driver)
        hb.windowsignin()
        
    def clickontravellers(self):
        hb=HomBusiclass(self.driver)
        hb.clickontravellers() 

    def clickontrips(self):
        time.sleep(5)
        print "enters in click on trips method"
        ab=AccBusiclass(self.driver)
        ab.clickontrips()
        print "After clicking on trips"
        time.sleep(5)             

    def getTrips(self):
        time.sleep(3)
        ab=AccBusiclass(self.driver)
        ab.getTripsBO()
        
    def getTravellers(self):
        ab=AccBusiclass(self.driver)
        ab.getTravellers()  
        
    def signout(self):
        ab=AccBusiclass(self.driver)
        ab.clickonsignout()                                 