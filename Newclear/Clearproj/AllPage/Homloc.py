'''
Created on Aug 20, 2017

@author: Ravikumar V
'''
from AllAct.CommActions import CommActionsclass
from selenium import webdriver
class Homlocclass(object):
    
    def __init__(self,driver):
        self.driver=driver
        
    def yourtripsloc(self):
        print " enters in click yourtrips"
        locatorstring="//li[@class='menuItem listMenuContainer userAccountMenuContainer']"
        ca=CommActionsclass(self.driver)
        ca.ElementwithFluentwait(locatorstring,60)
        yourtripsele=self.driver.find_element_by_xpath(locatorstring)
        return yourtripsele
    
    def travellersloc(self):
        print " enters in click yourtravellers"
        locatorstring="//li[@class='userAccountCol accountTools']/ul/li[2]/a"
        ca=CommActionsclass(self.driver)
        ca.ElementwithFluentwait(locatorstring,60)
        yourtripsele=self.driver.find_element_by_xpath(locatorstring)
        return yourtripsele
    
    def signinloc(self):
        locatorstring="//li[@class='signInBlock']/input"
        ca=CommActionsclass(self.driver)
        ca.ElementwithFluentwait(locatorstring,60)
        signinbutton=self.driver.find_element_by_xpath(locatorstring)
        return signinbutton
    def usernametext(self):
        self.driver.switch_to.frame("modal_window")
        locatorstring="//dd/input[@class='required email']"
        ca=CommActionsclass(self.driver)
        ca.ElementwithFluentwait(locatorstring,60)
        usernametext=self.driver.find_element_by_xpath(locatorstring)
        return usernametext
    def passwordtext(self):
        locatorstring="//dd/input[@class='required password']"
        ca=CommActionsclass(self.driver)
        ca.ElementwithFluentwait(locatorstring,60)
        passwordtext=self.driver.find_element_by_xpath(locatorstring)
        return passwordtext
    def windowsignin(self):
        locatorstring="//dd[@class='submit']/button"
        ca=CommActionsclass(self.driver)
        ca.ElementwithFluentwait(locatorstring,60)
        windowsignin=self.driver.find_element_by_xpath(locatorstring)
        return windowsignin
    def trips(self):
        locatorstring="//li/a/i[@class='iconSprite icoTrips']"
        ca=CommActionsclass(self.driver)
        ca.ElementwithFluentwait(locatorstring,90)
        trips=self.driver.find_element_by_xpath(locatorstring)
        return trips
        
        
    
        
