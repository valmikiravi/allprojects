'''
Created on Aug 20, 2017

@author: Ravikumar V
'''
from AllAct.CommActions import CommActionsclass
from selenium import webdriver

class Acclocclass(object):
    
    def __init__(self,driver):
        self.driver=driver
        
    def gettripsloc(self):
        locatorstring="//a[@class='clearFix']/div[1]/h3"    
        ca=CommActionsclass(self.driver)
        ca.ElementwithFluentwait(locatorstring,60)
        getripsele=self.driver.find_elements_by_xpath(locatorstring)
        return getripsele
    
    def clicktravellertab(self):
        locatorstring="//li[@class='travellersTab']"    
        ca=CommActionsclass(self.driver)
        ca.ElementwithFluentwait(locatorstring,60)
        clicktravellertab=self.driver.find_element_by_xpath(locatorstring)
        return clicktravellertab
    
    def gettravellers(self):
        locatorstring="//li[contains(@class,'person')]/a/div/h3"    
        ca=CommActionsclass(self.driver)
        ca.ElementwithFluentwait(locatorstring,60)
        gettravellersele=self.driver.find_element_by_xpath(locatorstring)
        return gettravellersele
    
    def signoutloc(self):
        locatorstring="//li[@class='miscLinks']/ul/li[3]/a"    
        ca=CommActionsclass(self.driver)
        ca.ElementwithFluentwait(locatorstring,60)
        getsignout=self.driver.find_element_by_xpath(locatorstring)
        return getsignout
    
    def headertextloc(self):
        locatorstring="//h1[contains(text(),'ve booked')]"    
        ca=CommActionsclass(self.driver)
        ca.ElementwithFluentwait(locatorstring,60)
        hederTEXTL=self.driver.find_element_by_xpath(locatorstring)
        return hederTEXTL
        



        