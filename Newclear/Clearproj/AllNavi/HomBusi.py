'''
Created on Aug 20, 2017

@author: Ravikumar V
'''
from AllAct.CommActions import CommActionsclass
from AllAct.HomActions import HomActionsclass
from selenium import webdriver

class HomBusiclass(object):
    
    def __init__(self,driver):
        self.driver=driver
        
    def clickonyourtrips(self):
        ha=HomActionsclass(self.driver)
        ha.clickonyourtrips()
        
    def clickontravellers(self):
        ha=HomActionsclass(self.driver)
        ha.clickontravellersAcc()    
        
    def clickonsignin(self):
        ha=HomActionsclass(self.driver)
        ha.clickonsignin()
        
    def enterusername(self,uNamE):
         ha=HomActionsclass(self.driver)
         ha.enterusername(uNamE)
    def enterpassword(self,pass_word1):
         ha=HomActionsclass(self.driver)
         ha.enterpassword(pass_word1)
    def windowsignin(self):
        ha=HomActionsclass(self.driver)
        ha.clickonwindowsignin()          